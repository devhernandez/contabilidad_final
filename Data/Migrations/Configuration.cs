namespace DAL.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using DomainModel;

    internal sealed class Configuration : DbMigrationsConfiguration<DAL.ContabilidadContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "DAL.ContabilidadContext";
        }

        protected override void Seed(DAL.ContabilidadContext context)
        {
            //Tipo Movimiento
            context.TipoMovimiento.AddOrUpdate(
                t => new { t.Id, t.Nombre, t.Estado },
                new TipoMovimiento { Id = 1, Nombre = "Debito", Estado = true },
                new TipoMovimiento { Id = 2, Nombre = "Credito", Estado = true }
            );

            //Tipo Cuenta

            //Tipo Moneda
            context.TipoMoneda.AddOrUpdate(
                x => new { x.Id, x.Descripcion, x.TasaActual, x.Formato },
                new TipoMoneda { Id = 1, Descripcion = "Peso", TasaActual = 1, Formato = "$RD" },
                new TipoMoneda { Id = 2, Descripcion = "Dollar", TasaActual = new decimal(47.89), Formato = "$USD" },
                new TipoMoneda { Id = 3, Descripcion = "Euro", TasaActual = new decimal(57.89), Formato = "$EUR" }
            );

            context.TipoCuenta.AddOrUpdate(
                x => new { x.Id, x.Nombre, x.Descripcion, x.NoControl, x.OrigenId },
                new TipoCuenta { Id = 1, Nombre = "Activos", Descripcion = "Activos", NoControl= 1,OrigenId = 1 },
                new TipoCuenta { Id = 2, Nombre = "Pasivos", Descripcion = "Pasivos", NoControl = 2, OrigenId = 2 },
                new TipoCuenta { Id = 3, Nombre = "Capital", Descripcion = "Capital", NoControl = 3, OrigenId = 2 },
                new TipoCuenta { Id = 4, Nombre = "Ingresos", Descripcion = "Ingresos", NoControl = 4, OrigenId = 2 },
                new TipoCuenta { Id = 5, Nombre = "Costos", Descripcion = "Costos", NoControl = 5, OrigenId = 1 },
                new TipoCuenta { Id = 6, Nombre = "Gastos", Descripcion = "Gastos", NoControl = 6, OrigenId = 1 }
            );

            // Auxiliar

            context.Auxiliar.AddOrUpdate(x => new { x.Id, x.Descripcion },
                new Auxiliar { Id = 1, Descripcion = "Contabilidad" },
                new Auxiliar { Id = 2, Descripcion = "Nomina" },
                new Auxiliar { Id = 3, Descripcion = "Facturacion" },
                new Auxiliar { Id = 4, Descripcion = "Inventario" },
                new Auxiliar { Id = 5, Descripcion = "Cuentas x Cobrar" },
                new Auxiliar { Id = 6, Descripcion = "Cuentas x Pagar" },
                new Auxiliar { Id = 7, Descripcion = "Compras" },
                new Auxiliar { Id = 8, Descripcion = "Activos Fijos" },
                new Auxiliar { Id = 9, Descripcion = "Cheques" });

             context.Login.AddOrUpdate(x=> new { x.Id,x.Usuario,x.Contrasena,x.TipoAcceso},
                 new Login {Id=1,Usuario="admin",Contrasena="admin1234",TipoAcceso= 3});

            //  This method wilId = 4,l be called after migrating to the latest version.

            //  You can use theId = 4, DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
