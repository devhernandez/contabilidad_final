namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class creacion : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Auxiliar",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descripcion = c.String(nullable: false, maxLength: 50, unicode: false),
                        Estado = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EntradaContable",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descripcion = c.String(nullable: false, unicode: false),
                        Fecha = c.DateTime(nullable: false),
                        Tasa = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AuxiliarId = c.Int(nullable: false),
                        MonedaId = c.Int(nullable: false),
                        Estatus = c.String(nullable: false),
                        Estado = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Auxiliar", t => t.AuxiliarId, cascadeDelete: true)
                .ForeignKey("dbo.TipoMoneda", t => t.MonedaId, cascadeDelete: true)
                .Index(t => t.AuxiliarId)
                .Index(t => t.MonedaId);
            
            CreateTable(
                "dbo.DetalleEntrada",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CuentaId = c.Int(nullable: false),
                        Credito = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Debito = c.Decimal(nullable: false, precision: 18, scale: 2),
                        EntradaContableId = c.Int(nullable: false),
                        Estado = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CuentaContable", t => t.CuentaId, cascadeDelete: true)
                .ForeignKey("dbo.EntradaContable", t => t.EntradaContableId, cascadeDelete: true)
                .Index(t => t.CuentaId)
                .Index(t => t.EntradaContableId);
            
            CreateTable(
                "dbo.CuentaContable",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descripcion = c.String(nullable: false, maxLength: 100, unicode: false),
                        PermiteTransacciones = c.Boolean(nullable: false),
                        Nivel = c.Int(nullable: false),
                        NoCuenta = c.String(nullable: false, maxLength: 50, unicode: false),
                        CuentaMayorId = c.Int(),
                        Balance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        NoControl = c.Int(nullable: false),
                        TipoCuentaId = c.Int(nullable: false),
                        Estado = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CuentaContable", t => t.CuentaMayorId)
                .ForeignKey("dbo.TipoCuenta", t => t.TipoCuentaId, cascadeDelete: true)
                .Index(t => t.CuentaMayorId)
                .Index(t => t.TipoCuentaId);
            
            CreateTable(
                "dbo.TipoCuenta",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 100, unicode: false),
                        Descripcion = c.String(nullable: false, maxLength: 100, unicode: false),
                        NoControl = c.Int(nullable: false),
                        OrigenId = c.Int(nullable: false),
                        Estado = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TipoMovimiento", t => t.OrigenId, cascadeDelete: true)
                .Index(t => t.OrigenId);
            
            CreateTable(
                "dbo.TipoMovimiento",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 50, unicode: false),
                        Estado = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TipoMoneda",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Formato = c.String(nullable: false, maxLength: 100, unicode: false),
                        Descripcion = c.String(nullable: false, maxLength: 100, unicode: false),
                        TasaActual = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Estado = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Login",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Usuario = c.String(nullable: false, maxLength: 100, unicode: false),
                        Contrasena = c.String(nullable: false, maxLength: 100),
                        TipoAcceso = c.Int(nullable: false),
                        Estado = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Mayorizacion",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Fecha = c.DateTime(nullable: false, storeType: "date"),
                        Balance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Cuenta = c.Int(nullable: false),
                        TipoMovimiento = c.Int(nullable: false),
                        Estado = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EntradaContable", "MonedaId", "dbo.TipoMoneda");
            DropForeignKey("dbo.DetalleEntrada", "EntradaContableId", "dbo.EntradaContable");
            DropForeignKey("dbo.DetalleEntrada", "CuentaId", "dbo.CuentaContable");
            DropForeignKey("dbo.TipoCuenta", "OrigenId", "dbo.TipoMovimiento");
            DropForeignKey("dbo.CuentaContable", "TipoCuentaId", "dbo.TipoCuenta");
            DropForeignKey("dbo.CuentaContable", "CuentaMayorId", "dbo.CuentaContable");
            DropForeignKey("dbo.EntradaContable", "AuxiliarId", "dbo.Auxiliar");
            DropIndex("dbo.TipoCuenta", new[] { "OrigenId" });
            DropIndex("dbo.CuentaContable", new[] { "TipoCuentaId" });
            DropIndex("dbo.CuentaContable", new[] { "CuentaMayorId" });
            DropIndex("dbo.DetalleEntrada", new[] { "EntradaContableId" });
            DropIndex("dbo.DetalleEntrada", new[] { "CuentaId" });
            DropIndex("dbo.EntradaContable", new[] { "MonedaId" });
            DropIndex("dbo.EntradaContable", new[] { "AuxiliarId" });
            DropTable("dbo.Mayorizacion");
            DropTable("dbo.Login");
            DropTable("dbo.TipoMoneda");
            DropTable("dbo.TipoMovimiento");
            DropTable("dbo.TipoCuenta");
            DropTable("dbo.CuentaContable");
            DropTable("dbo.DetalleEntrada");
            DropTable("dbo.EntradaContable");
            DropTable("dbo.Auxiliar");
        }
    }
}
