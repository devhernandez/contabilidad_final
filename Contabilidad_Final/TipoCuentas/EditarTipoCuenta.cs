﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Contabilidad_App.Utils;
using MetroFramework;
using MetroFramework.Forms;
using Service.TiposCuenta;
using System.Linq;

namespace Contabilidad_App.TipoCuentas
{

    public partial class EditarTipoCuenta : MetroForm
    {
        public DomainModel.TipoCuenta TipoCuenta { get; set; }
        private readonly ITipoCuentaService _tipoCuentaService;
        public EditarTipoCuenta()
        {
            InitializeComponent();
            CargarEstado();
            CargarTipoMovimiento();
            _tipoCuentaService = new TipoCuentaService();
            UltimoNumeroDeControl();
        }

        private void CargarEstado()
        {
            var estados = new Dictionary<bool, string> { { true, "Activo" }, { false, "Inactivo" } };
            cbxEstado.DataSource = new BindingSource(estados, null);
            cbxEstado.DisplayMember = "Value";
            cbxEstado.ValueMember = "Key";
        }

        private void UltimoNumeroDeControl(){

            try
            {
                var cuentas = _tipoCuentaService.GetAll();

                var maxNo = cuentas.Max(r => r.NoControl);
                txtNoControl.Text = (maxNo +1 ).ToString();
            }
            catch (Exception ex ) {

                MetroMessageBox.Show(this, ex.Message, "Tipo Cuenta", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }


        }

        private void CargarTipoMovimiento()
        {
            var data = new Dictionary<int, string> { { 1, "Debito" }, { 2, "Credito" } };
            cbxOrigen.DataSource = new BindingSource(data, null);
            cbxOrigen.DisplayMember = "Value";
            cbxOrigen.ValueMember = "Key";
        }

        #region Event
        private void EditarTipoCuenta_Load(object sender, EventArgs e)
        {
            if (TipoCuenta == null) return;

            txtId.Text = TipoCuenta.Id.ToString();
            txtNombre.Text = TipoCuenta.Nombre;
            txtDescripcion.Text = TipoCuenta.Descripcion;
            cbxOrigen.SelectedValue = TipoCuenta.OrigenId;
            txtNoControl.Text = TipoCuenta.NoControl.ToString();
            cbxEstado.SelectedValue = TipoCuenta.Estado;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Agregar();
                MetroMessageBox.Show(this, "Los datos se guardaron correctamente.","Tipo Cuenta", MessageBoxButtons.OK, MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
                Close();
            }
            catch(Exception ex)
            {
                MetroMessageBox.Show(this, ex.Message, "Tipo Cuenta", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                Inactivar();
                MetroMessageBox.Show(this, "Los datos se eliminaron correctamente.", "Tipo Cuenta", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
                Close();
            }
            catch (Exception ex)
            {
                MetroMessageBox.Show(this, ex.Message, "Tipo Cuenta", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region CRUD

        private void Add(DomainModel.TipoCuenta tipoCuenta)
        {
            _tipoCuentaService.Save(tipoCuenta);
        }

        #endregion

        #region Action

        private void Agregar()
        {
            var tipoCuenta = Map;
             Add(tipoCuenta);

        }

        private void Inactivar()
        {
            var tipoCuenta = Map;
            _tipoCuentaService.Delete(tipoCuenta.Id);
        }

        private DomainModel.TipoCuenta Map
        {
            get
            {
                var tipoCuenta = new DomainModel.TipoCuenta
                {
                    Id = int.Parse(txtId.Text),
                    Nombre = txtNombre.Text,
                    Descripcion = txtDescripcion.Text,
                    NoControl = int.Parse(txtNoControl.Text),
                    OrigenId = (int)cbxOrigen.SelectedValue,
                    Estado = (bool)cbxEstado.SelectedValue
                };
                return tipoCuenta;
            }
        }


        #endregion

        private void txtNoControl_KeyPress(object sender, KeyPressEventArgs e)
        {
            var validator = new TextBoxValidator();
            validator.IsInteger(sender,  e);
        }
    }
}
