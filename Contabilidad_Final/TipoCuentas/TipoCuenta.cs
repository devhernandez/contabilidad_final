﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using MetroFramework.Controls;
using Service.TiposCuenta;

namespace Contabilidad_App.TipoCuentas
{
    public partial class TipoCuenta : MetroUserControl
    {
        private ITipoCuentaService _tipoCuentaService;
        public TipoCuenta()
        {
            InitializeComponent();

        }

        private void CargarDatos(IEnumerable<DomainModel.TipoCuenta> tipoCuentas)
        {
            dgvTiposCuentas.Rows.Clear();
            foreach (var tipoCuenta in tipoCuentas)
            {
                var estado = tipoCuenta.Estado ? "Activo" : "Inactivo";
                dgvTiposCuentas.Rows.Add(tipoCuenta.Id, tipoCuenta.Nombre, tipoCuenta.Descripcion, tipoCuenta.Origen.Nombre,tipoCuenta.NoControl, estado);
            }
        }

        private DomainModel.TipoCuenta GetTipoCuenta()
        {
            var tipoCuenta = new DomainModel.TipoCuenta();
            var row = dgvTiposCuentas.SelectedRows[0];
            if (row.Cells[0].Value == null)
                return tipoCuenta;

            var model = _tipoCuentaService.GetOne(Convert.ToInt32(row.Cells[0].Value.ToString()));
            tipoCuenta = model;

            return tipoCuenta;
        }

        #region Event

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            var formulario = new EditarTipoCuenta { Text = "Agregar Tipo Cuenta" };
            formulario.ShowDialog();
        }
        private void TipoCuenta_Load(object sender, EventArgs e)
        {
            _tipoCuentaService = new TipoCuentaService();
            CargarDatos(_tipoCuentaService.GetAll());

        }
        private void dgvTiposCuentas_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var formulario = new EditarTipoCuenta();
            formulario.ShowDialog();
        }
        private void dgvTiposCuentas_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var tipoCuenta = GetTipoCuenta();
            var formulario = new EditarTipoCuenta { Text = "Editar Tipo Cuenta", TipoCuenta = tipoCuenta };
            var resultado =formulario.ShowDialog();

            if (resultado == DialogResult.OK)
            {
                Reload();
                dgvTiposCuentas.Refresh();
            }
        }
        private void btnBuscar_Click(object sender, EventArgs e)
        {
            var filtro = txtFiltro.Text;
            var tiposCuentas = _tipoCuentaService.Search(filtro);
            CargarDatos(tiposCuentas);

        }
        #endregion

        private void Reload()
        {
            CargarDatos(_tipoCuentaService.GetAll());
        }

        private void dgvTiposCuentas_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
