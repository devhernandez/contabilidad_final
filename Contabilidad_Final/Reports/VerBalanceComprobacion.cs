﻿using System;
using System.Data;
using System.Linq;
using DomainModel;
using MetroFramework.Forms;
using Microsoft.Reporting.WinForms;
using Service.DetallesContables;

namespace Contabilidad_App.Reports
{
    public partial class VerBalanceComprobacion : MetroForm
    {
        public VerBalanceComprobacion()
        {
            InitializeComponent();
        }

        private void VerBalanceComprobacion_Load(object sender, System.EventArgs e)
        {
            reportViewer1.ProcessingMode = ProcessingMode.Local;

            var report = reportViewer1.LocalReport;
            report.ReportPath = "Reports/BalanceComprobacionA.rdlc";
            report.ReportEmbeddedResource = "Reports/BalanceComprobacionA.rdlc";

            var dataSet = new DataSet("Personal");



            var fechaInicio = new DateTime(2017, 06, 01);
            var fechaTermino = new DateTime(2017, 07, 01);

            var data = new DetalleContableService().GetAll(x => x.EntradaContable.Fecha >= fechaInicio && x.EntradaContable.Fecha <= fechaTermino)
                .Select(x => new { x.Cuenta.Descripcion, x.Debito, x.Credito });

            var dataSource = new ReportDataSource("DataSet1", data);

            report.DataSources.Add(dataSource);

            var fechaInicial = new ReportParameter("FechaInicio", fechaInicio.ToShortDateString());
            var fechaFinal = new ReportParameter("FechaTermino", fechaTermino.ToShortDateString());

            report.SetParameters(new[] { fechaInicial, fechaFinal });

            report.Refresh();
            reportViewer1.RefreshReport();
        }

        private void GetBalancedeComprobacion(DateTime fechaInicio, DateTime fechaTermino)
        {
        }
    }
}
