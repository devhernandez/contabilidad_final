﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;
using Microsoft.Reporting.WinForms;
using Service.DetallesContables;

namespace Contabilidad_App.Reports
{
    public partial class BalanceDeComprobacion : MetroUserControl
    {
        public BalanceDeComprobacion()
        {
            InitializeComponent();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            reportViewer1.Reset();
            reportViewer1.ProcessingMode = ProcessingMode.Local;

            var report = reportViewer1.LocalReport;
            report.ReportPath = "Reports/BalanceComprobacionA.rdlc";
            report.ReportEmbeddedResource = "Reports/BalanceComprobacionA.rdlc";

            var dataSet = new DataSet("Personal");

            var fechaInicio = DateTime.Parse(txtFechaInicio.Text);
            var fechaTermino = DateTime.Parse(txtFechaTermino.Text);

            var data2= new DetalleContableService().GetAll();

            var data = new DetalleContableService().GetAll(x => x.EntradaContable.Fecha >= fechaInicio && x.EntradaContable.Fecha <= fechaTermino)
                .Select(x => new { x.Cuenta.Descripcion, x.Debito, x.Credito });

            var dataSource = new ReportDataSource("DataSet1", data);

            report.DataSources.Add(dataSource);

            var fechaInicial = new ReportParameter("FechaInicio", fechaInicio.ToShortDateString());
            var fechaFinal = new ReportParameter("FechaTermino", fechaTermino.ToShortDateString());

            report.SetParameters(new[] { fechaInicial, fechaFinal });

            reportViewer1.RefreshReport();
        }


    }
}
