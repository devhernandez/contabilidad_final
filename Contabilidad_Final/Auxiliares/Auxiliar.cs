﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using MetroFramework.Controls;
using Service.Auxiliares;
using DomainModel;

namespace Contabilidad_App.Auxiliares
{
    public partial class Auxiliar : MetroUserControl
    {
        private IAuxiliarService _auxiliarService;
       // private readonly IAuxiliarService cargarDatos = new AuxiliarService();
      
        public Auxiliar()
        {
            InitializeComponent();

            
            // _auxiliarService = new AuxiliarService();
            //  CargarDatos(_auxiliarService.GetAll());
            //  CargarDatos(cargarDatos.GetAll());
        }

        private void CargarDatos(IEnumerable<DomainModel.Auxiliar> auxiliares)
        {
            dgvAuxiliares.Rows.Clear();
            foreach (var model in auxiliares)
            {
                var estado = model.Estado ? "Activo" : "Inactivo";
                dgvAuxiliares.Rows.Add(model.Id, model.Descripcion, estado);
            }
        }

        private DomainModel.Auxiliar GetAuxiliar()
        {
            var auxiliar = new DomainModel.Auxiliar();
            var row = dgvAuxiliares.SelectedRows[0];
            if (row.Cells[0].Value == null)
                return auxiliar;

            auxiliar = _auxiliarService.GetOne(Convert.ToInt32(row.Cells[0].Value.ToString()));
            return auxiliar;
        }

        private void Reload()
        {
            CargarDatos(_auxiliarService.GetAll());
        }

        private void Auxiliar_Load(object sender, EventArgs e)
        {

            _auxiliarService = new AuxiliarService();
            CargarDatos(_auxiliarService.GetAll());
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            var filtro = txtFiltro.Text;
            var auxiliares = _auxiliarService.Search(filtro);
            CargarDatos(auxiliares);
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
             var formulario = new AddAuxiliar { Text = "Agregar Auxiliar" };
            var resultado = formulario.ShowDialog();

            if (resultado == DialogResult.OK)
            {
                Reload();
                dgvAuxiliares.Refresh();
            }
        }

        private void dgvAuxiliares_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var tipoCuenta = GetAuxiliar();
            var formulario = new AddAuxiliar{ Text = "Editar Auxiliar", Auxiliar = tipoCuenta };
            var resultado = formulario.ShowDialog();

            if (resultado == DialogResult.OK)
            {
                Reload();
                dgvAuxiliares.Refresh();
            }
        }
    }
}
