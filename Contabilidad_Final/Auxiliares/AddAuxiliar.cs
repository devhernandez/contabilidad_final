﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using MetroFramework;
using MetroFramework.Forms;
using Service.Auxiliares;

namespace Contabilidad_App.Auxiliares
{
    public partial class AddAuxiliar : MetroForm
    {
        public DomainModel.Auxiliar Auxiliar { get; set; }
        private readonly IAuxiliarService _auxiliarService;
        public AddAuxiliar()
        {
            InitializeComponent();
            CargarEstado();
            _auxiliarService = new AuxiliarService();
        }

        private void CargarEstado()
        {
            var estados = new Dictionary<bool, string> { { true, "Activo" }, { false, "Inactivo" } };
            cbxEstado.DataSource = new BindingSource(estados, null);
            cbxEstado.DisplayMember = "Value";
            cbxEstado.ValueMember = "Key";
        }


        #region CRUD

        private void Add(DomainModel.Auxiliar auxiliar)
        {
            _auxiliarService.Save(auxiliar);
        }

        #endregion

        #region Action

        private void Agregar()
        {
            var tipoCuenta = Map;
            Add(tipoCuenta);

        }

        private void Inactivar()
        {
            var tipoCuenta = Map;
            _auxiliarService.Delete(tipoCuenta.Id);
        }

        private DomainModel.Auxiliar Map
        {
            get
            {
                var auxiliar = new DomainModel.Auxiliar
                {
                    Id = int.Parse(txtId.Text),
                    Descripcion = txtDescripcion.Text,
                    Estado = (bool)cbxEstado.SelectedValue
                };
                return auxiliar;
            }
        }

        private void ReverseMap()
        {
            if (Auxiliar == null) return;

            txtId.Text = Auxiliar.Id.ToString();
            txtDescripcion.Text = Auxiliar.Descripcion;
            cbxEstado.SelectedValue = Auxiliar.Estado;
        }

        #endregion

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Agregar();
                MetroMessageBox.Show(this, "Los datos se guardaron correctamente.", "Auxiliar", MessageBoxButtons.OK, MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
                Close();
            }
            catch (Exception ex)
            {
                MetroMessageBox.Show(this, ex.Message, "Auxiliar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                Inactivar();
                MetroMessageBox.Show(this, "Los datos se eliminaron correctamente.", "Auxiliar", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
                Close();
            }
            catch (Exception ex)
            {
                MetroMessageBox.Show(this, ex.Message, "Auxiliar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void AddAuxiliar_Load(object sender, EventArgs e)
        {
            if (Auxiliar == null) return;

            txtId.Text = Auxiliar.Id.ToString();   
            txtDescripcion.Text = Auxiliar.Descripcion;
            cbxEstado.SelectedValue = Auxiliar.Estado;
        }
    }
}
