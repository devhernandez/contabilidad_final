﻿namespace Contabilidad_App
{
    partial class Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroTabControl1 = new MetroFramework.Controls.MetroTabControl();
            this.metroTabPage1 = new MetroFramework.Controls.MetroTabPage();
            this.tipoCuenta1 = new Contabilidad_App.TipoCuentas.TipoCuenta();
            this.metroTabPage2 = new MetroFramework.Controls.MetroTabPage();
            this.cuentaContable1 = new Contabilidad_App.CuentasContables.CuentaContable();
            this.metroTabPage4 = new MetroFramework.Controls.MetroTabPage();
            this.auxiliar1 = new Contabilidad_App.Auxiliares.Auxiliar();
            this.metroTabPage3 = new MetroFramework.Controls.MetroTabPage();
            this.moneda1 = new Contabilidad_App.TipoMonedas.Moneda();
            this.metroTabPage5 = new MetroFramework.Controls.MetroTabPage();
            this.asiento1 = new Contabilidad_App.EntradasContable.Asiento();
            this.metroTabPage6 = new MetroFramework.Controls.MetroTabPage();
            this.accesos1 = new Contabilidad_App.Acceso.Accesos();
            this.metroTabPage7 = new MetroFramework.Controls.MetroTabPage();
            this.reporte1 = new Contabilidad_App.Reports.BalanceDeComprobacion();
            this.label1 = new System.Windows.Forms.Label();
            this.txtActualUser = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.metroTabControl1.SuspendLayout();
            this.metroTabPage1.SuspendLayout();
            this.metroTabPage2.SuspendLayout();
            this.metroTabPage4.SuspendLayout();
            this.metroTabPage3.SuspendLayout();
            this.metroTabPage5.SuspendLayout();
            this.metroTabPage6.SuspendLayout();
            this.metroTabPage7.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroTabControl1
            // 
            this.metroTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroTabControl1.Controls.Add(this.metroTabPage1);
            this.metroTabControl1.Controls.Add(this.metroTabPage2);
            this.metroTabControl1.Controls.Add(this.metroTabPage4);
            this.metroTabControl1.Controls.Add(this.metroTabPage3);
            this.metroTabControl1.Controls.Add(this.metroTabPage5);
            this.metroTabControl1.Controls.Add(this.metroTabPage7);
            this.metroTabControl1.Controls.Add(this.metroTabPage6);
            this.metroTabControl1.Location = new System.Drawing.Point(20, 60);
            this.metroTabControl1.Name = "metroTabControl1";
            this.metroTabControl1.SelectedIndex = 0;
            this.metroTabControl1.Size = new System.Drawing.Size(853, 485);
            this.metroTabControl1.Style = MetroFramework.MetroColorStyle.Green;
            this.metroTabControl1.TabIndex = 0;
            this.metroTabControl1.UseSelectable = true;
            // 
            // metroTabPage1
            // 
            this.metroTabPage1.Controls.Add(this.tipoCuenta1);
            this.metroTabPage1.HorizontalScrollbarBarColor = true;
            this.metroTabPage1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.HorizontalScrollbarSize = 10;
            this.metroTabPage1.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage1.Name = "metroTabPage1";
            this.metroTabPage1.Size = new System.Drawing.Size(845, 443);
            this.metroTabPage1.TabIndex = 0;
            this.metroTabPage1.Text = "Cuentas";
            this.metroTabPage1.VerticalScrollbarBarColor = true;
            this.metroTabPage1.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.VerticalScrollbarSize = 10;
            // 
            // tipoCuenta1
            // 
            this.tipoCuenta1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tipoCuenta1.AutoSize = true;
            this.tipoCuenta1.Location = new System.Drawing.Point(-4, 3);
            this.tipoCuenta1.Name = "tipoCuenta1";
            this.tipoCuenta1.Size = new System.Drawing.Size(878, 427);
            this.tipoCuenta1.TabIndex = 2;
            this.tipoCuenta1.UseSelectable = true;
            // 
            // metroTabPage2
            // 
            this.metroTabPage2.HorizontalScrollbarBarColor = true;
            this.metroTabPage2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage2.HorizontalScrollbarSize = 10;
            this.metroTabPage2.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage2.Name = "metroTabPage2";
            this.metroTabPage2.Size = new System.Drawing.Size(843, 443);
            this.metroTabPage2.TabIndex = 1;
            this.metroTabPage2.Text = "Cuentas Contables";
            this.metroTabPage2.VerticalScrollbarBarColor = true;
            this.metroTabPage2.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage2.VerticalScrollbarSize = 10;

            this.metroTabPage2.Controls.Add(this.cuentaContable1);
            this.cuentaContable1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                | System.Windows.Forms.AnchorStyles.Left)
             | System.Windows.Forms.AnchorStyles.Right)));
            this.cuentaContable1.AutoSize = true;
            this.cuentaContable1.Location = new System.Drawing.Point(-4, 3);
            this.cuentaContable1.Name = "cuentaContable1";
            this.cuentaContable1.Size = new System.Drawing.Size(878, 427);
            this.cuentaContable1.TabIndex = 2;
            this.cuentaContable1.UseSelectable = true;


            // 
            // metroTabPage4
            // 
            this.metroTabPage4.Controls.Add(this.auxiliar1);
            this.metroTabPage4.HorizontalScrollbarBarColor = true;
            this.metroTabPage4.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage4.HorizontalScrollbarSize = 10;
            this.metroTabPage4.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage4.Name = "metroTabPage4";
            this.metroTabPage4.Size = new System.Drawing.Size(843, 443);
            this.metroTabPage4.TabIndex = 3;
            this.metroTabPage4.Text = "Cuenta Auxiliares";
            this.metroTabPage4.VerticalScrollbarBarColor = true;
            this.metroTabPage4.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage4.VerticalScrollbarSize = 10;
            // 
            // auxiliar1
            // 
            this.auxiliar1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.auxiliar1.AutoSize = true;
            this.auxiliar1.Location = new System.Drawing.Point(-4, 3);
            this.auxiliar1.Name = "auxiliar1";
            this.auxiliar1.Size = new System.Drawing.Size(1264, 658);
            this.auxiliar1.TabIndex = 2;
            this.auxiliar1.UseSelectable = true;
            // 
            // metroTabPage3
            // 
            this.metroTabPage3.Controls.Add(this.moneda1);
            this.metroTabPage3.HorizontalScrollbarBarColor = true;
            this.metroTabPage3.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage3.HorizontalScrollbarSize = 10;
            this.metroTabPage3.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage3.Name = "metroTabPage3";
            this.metroTabPage3.Size = new System.Drawing.Size(843, 443);
            this.metroTabPage3.TabIndex = 2;
            this.metroTabPage3.Text = "Monedas";
            this.metroTabPage3.VerticalScrollbarBarColor = true;
            this.metroTabPage3.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage3.VerticalScrollbarSize = 10;
            // 
            // moneda1
            // 
            this.moneda1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.moneda1.AutoSize = true;
            this.moneda1.Location = new System.Drawing.Point(-4, 3);
            this.moneda1.Name = "moneda1";
            this.moneda1.Size = new System.Drawing.Size(1093, 502);
            this.moneda1.TabIndex = 2;
            this.moneda1.UseSelectable = true;
            // 
            // metroTabPage5
            // 
            this.metroTabPage5.Controls.Add(this.asiento1);
            this.metroTabPage5.HorizontalScrollbarBarColor = true;
            this.metroTabPage5.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage5.HorizontalScrollbarSize = 10;
            this.metroTabPage5.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage5.Name = "metroTabPage5";
            this.metroTabPage5.Size = new System.Drawing.Size(843, 443);
            this.metroTabPage5.TabIndex = 4;
            this.metroTabPage5.Text = "Entradas";
            this.metroTabPage5.VerticalScrollbarBarColor = true;
            this.metroTabPage5.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage5.VerticalScrollbarSize = 10;
            // 
            // asiento1
            // 
            this.asiento1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.asiento1.AutoSize = true;
            this.asiento1.Location = new System.Drawing.Point(-74, 3);
            this.asiento1.Name = "asiento1";
            this.asiento1.Size = new System.Drawing.Size(1346, 678);
            this.asiento1.TabIndex = 2;
            this.asiento1.UseSelectable = true;
            // 
            // metroTabPage6
            // 
            this.metroTabPage6.Controls.Add(this.accesos1);
            this.metroTabPage6.HorizontalScrollbarBarColor = true;
            this.metroTabPage6.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage6.HorizontalScrollbarSize = 10;
            this.metroTabPage6.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage6.Name = "metroTabPage6";
            this.metroTabPage6.Size = new System.Drawing.Size(843, 443);
            this.metroTabPage6.TabIndex = 5;
            this.metroTabPage6.Text = "Ajustes";
            this.metroTabPage6.VerticalScrollbarBarColor = true;
            this.metroTabPage6.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage6.VerticalScrollbarSize = 10;
            // 
            // accesos1
            // 
            this.accesos1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.accesos1.AutoSize = true;
            this.accesos1.Location = new System.Drawing.Point(-4, 3);
            this.accesos1.Name = "accesos1";
            this.accesos1.Size = new System.Drawing.Size(1218, 732);
            this.accesos1.TabIndex = 2;
            this.accesos1.UseSelectable = true;
            // 
            // metroTabPage7
            // 
            this.metroTabPage7.Controls.Add(this.reporte1);
            this.metroTabPage7.HorizontalScrollbarBarColor = true;
            this.metroTabPage7.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage7.HorizontalScrollbarSize = 10;
            this.metroTabPage7.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage7.Name = "metroTabPage7";
            this.metroTabPage7.Size = new System.Drawing.Size(813, 434);
            this.metroTabPage7.TabIndex = 6;
            this.metroTabPage7.Text = "Reportes";
            this.metroTabPage7.VerticalScrollbarBarColor = true;
            this.metroTabPage7.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage7.VerticalScrollbarSize = 10;
            // 
            // reporte1
            // 
            this.reporte1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.reporte1.AutoSize = true;
            this.reporte1.Location = new System.Drawing.Point(-4, 3);
            this.reporte1.Name = "reporte1";
            this.reporte1.Size = new System.Drawing.Size(1186, 723);
            this.reporte1.TabIndex = 2;
            this.reporte1.UseSelectable = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(618, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Usuario: ";
            // 
            // txtActualUser
            // 
            this.txtActualUser.Location = new System.Drawing.Point(673, 21);
            this.txtActualUser.Name = "txtActualUser";
            this.txtActualUser.ReadOnly = true;
            this.txtActualUser.Size = new System.Drawing.Size(141, 20);
            this.txtActualUser.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.BackgroundImage = global::Contabilidad_App.Properties.Resources.logout_512;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Location = new System.Drawing.Point(673, 47);
            this.button1.Name = "button1";
            this.button1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.button1.Size = new System.Drawing.Size(60, 45);
            this.button1.TabIndex = 3;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = MetroFramework.Forms.MetroFormBorderStyle.FixedSingle;
            this.ClientSize = new System.Drawing.Size(909, 565);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtActualUser);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.metroTabControl1);
            this.Name = "Home";
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.DropShadow;
            this.Style = MetroFramework.MetroColorStyle.Green;
            this.Text = "Aplicacion de Contabilidad Financiera";
            this.Theme = MetroFramework.MetroThemeStyle.Default;
            this.Load += new System.EventHandler(this.Home_Load);
            this.metroTabControl1.ResumeLayout(false);
            this.metroTabPage1.ResumeLayout(false);
            this.metroTabPage1.PerformLayout();
            this.metroTabPage2.ResumeLayout(false);
            this.metroTabPage2.PerformLayout();
            this.metroTabPage4.ResumeLayout(false);
            this.metroTabPage4.PerformLayout();
            this.metroTabPage3.ResumeLayout(false);
            this.metroTabPage3.PerformLayout();
            this.metroTabPage5.ResumeLayout(false);
            this.metroTabPage5.PerformLayout();
            this.metroTabPage6.ResumeLayout(false);
            this.metroTabPage6.PerformLayout();
            this.metroTabPage7.ResumeLayout(false);
            this.metroTabPage7.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroTabControl metroTabControl1;
        private MetroFramework.Controls.MetroTabPage metroTabPage2;
        private MetroFramework.Controls.MetroTabPage metroTabPage1;
        private MetroFramework.Controls.MetroTabPage metroTabPage3;
        private MetroFramework.Controls.MetroTabPage metroTabPage4;
        private MetroFramework.Controls.MetroTabPage metroTabPage5;
        private MetroFramework.Controls.MetroTabPage metroTabPage6;
        private MetroFramework.Controls.MetroTabPage metroTabPage7;
        private TipoCuentas.TipoCuenta tipoCuenta1;
        private TipoMonedas.Moneda moneda1;
        private Auxiliares.Auxiliar auxiliar1;
        private CuentasContables.CuentaContable cuentaContable1;
        private EntradasContable.Asiento asiento1;
        private Acceso.Accesos accesos1;
        private Reports.BalanceDeComprobacion reporte1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtActualUser;
        private System.Windows.Forms.Button button1;
    }
}

