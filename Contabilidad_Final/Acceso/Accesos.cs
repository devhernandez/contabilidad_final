﻿using MetroFramework.Controls;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Service.Accesos;
namespace Contabilidad_App.Acceso
{
    public partial class Accesos : MetroUserControl
    {
        private  IAccesosService _accesosService;
        public Accesos()
        {
            InitializeComponent();
            
        }


        private void CargarDatos(IEnumerable<DomainModel.Login> usuarios)
        {
            dgvAccesos.Rows.Clear();
            foreach (var model in usuarios)
            {
                var estado = model.Estado ? "Activo" : "Inactivo";
                var acceso = "";
                switch (model.TipoAcceso ) {
                    case 1:
                        acceso = "Contable";
                        break;
                    case 2:
                        acceso = "Accesor";
                        break;
                    case 3:
                       acceso= "Administrador";
                        break;
                }
                dgvAccesos.Rows.Add(model.Id, model.Usuario,acceso,estado);
            }
        }

        private DomainModel.Login GetLogin()
        {
            var users = new DomainModel.Login();
            var row = dgvAccesos.SelectedRows[0];
            if (row.Cells[0].Value == null)
                return users;

            users = _accesosService.GetOne(Convert.ToInt32(row.Cells[0].Value.ToString()));
            return users;
        }

        private void Reload()
        {
            CargarDatos(_accesosService.GetAll());
        }


        private void btnAgregar_Click(object sender, EventArgs e)
        {
            var formulario = new AddAcceso { Text = "Agregar Accesos" };
            var resultado = formulario.ShowDialog();

            if (resultado == DialogResult.OK)
            {
                Reload();
                dgvAccesos.Refresh();
            }
        }

        private void dgvAccesos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var login= GetLogin();
            var formulario = new AddAcceso { Text = "Editar Usuario", acceso = login };
            var resultado = formulario.ShowDialog();

            if (resultado == DialogResult.OK)
            {
                Reload();
                dgvAccesos.Refresh();
            }
        }

        private void Accesos_Load(object sender, EventArgs e)
        {
            _accesosService = new AccesosService();
            CargarDatos(_accesosService.GetAll());
        }
    }
}
