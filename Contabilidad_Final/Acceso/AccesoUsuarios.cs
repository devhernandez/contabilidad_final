﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;
using MetroFramework.Forms;
using Service.Accesos;
using DomainModel;

namespace Contabilidad_App
{
    public partial class AccesoUsuarios : MetroForm
    {   
        private IAccesosService _accesosService;
        private Login UsuariosExistoso;
        public AccesoUsuarios()
        {
            InitializeComponent();
            _accesosService = new AccesosService();
    }

        private void AccesoUsuarios_Load(object sender, EventArgs e)
        {
            textUser.Focus();
        }

        private void btniniciar_Click(object sender, EventArgs e)
        {


            try {



                if (textUser.Text == "")
                {
                    textUser.Focus();
                    MessageBox.Show("Por Favor Introduza su usuario");
                    return;
                }

                if (txtContra.Text == "") {
                    txtContra.Focus();
                    MessageBox.Show("Por Favor Introduza su contraseña");
                    return;
                }

               var access= Acceso(textUser.Text, txtContra.Text, _accesosService.GetAll());

                if (access) {

                    MessageBox.Show("Bienvenido "+textUser.Text+" ! ");
                    var menu = new Home();
                    menu.NivelAcceso = UsuariosExistoso.TipoAcceso;
                    menu.ActualUser = textUser.Text;
                    menu.Show();
                    
                    this.Hide();
                }
              

            } catch (Exception ex) {
                MessageBox.Show("Error al Iniciar Sesion");
            }



        }

        private bool Acceso(string usuario,string contra,IEnumerable<DomainModel.Login> usuarios)
        {
            bool usuarioExiste = false;
            foreach (var model in usuarios)
            {
                usuarioExiste = true;
                if (model.Usuario == usuario)
                {
                    if (model.Estado == false) {
                        MessageBox.Show("Usuario Inactivo");
                        return false;
                    }

                    if (model.Contrasena == contra) {
                        UsuariosExistoso = model;

                        return true;
                    }
                }

            }

            if (usuarioExiste == true)
            {
                MessageBox.Show("La Contraseña esta incorrecta");
            }
            else {
                MessageBox.Show("El Usuario No existe");
            }


            return false;
        }

        private void textBoxTest_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btniniciar_Click(this, new EventArgs());
            }
        }

  }
}
