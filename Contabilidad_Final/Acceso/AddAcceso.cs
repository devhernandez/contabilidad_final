﻿using MetroFramework.Forms;
using System;
using System.Windows.Forms;
using MetroFramework;
using DomainModel;
using Service.Accesos;
using System.Collections.Generic;

namespace Contabilidad_App.Acceso
{
    public partial class AddAcceso : MetroForm
    {
        public Login acceso { get; set; }
       
        private readonly IAccesosService _accesosService;
        public AddAcceso()
        {
            InitializeComponent();
            CargarTipoAcceso();
            CargarEstado();
            _accesosService = new AccesosService();
        }

        private void CargarEstado()
        {
            var estados = new Dictionary<bool, string> { { true, "Activo" }, { false, "Inactivo" } };
            cbxEstado.DataSource = new BindingSource(estados, null);
            cbxEstado.DisplayMember = "Value";
            cbxEstado.ValueMember = "Key";
        }

        private void CargarTipoAcceso()
        {
            var access = new Dictionary<int, string> { { 1, "Contable" }, { 2, "Accesor" }, {3,"Administrador" } };
            cbxTipoAcceso.DataSource = new BindingSource(access, null);
            cbxTipoAcceso.DisplayMember = "Value";
            cbxTipoAcceso.ValueMember = "Key";
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Agregar();
                MetroMessageBox.Show(this, "Listo !.", "Accesos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
                Close();
            }
            catch (Exception ex)
            {
                MetroMessageBox.Show(this, ex.Message, "Accesos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #region CRUD

        private void Add(DomainModel.Login user)
        {
            _accesosService.Save(user);
        }

        #endregion

        #region Action

        private void Agregar()
        {
            var acceso = Map;
            Add(acceso);

        }

        private void Inactivar()
        {
            var acceso = Map;
            _accesosService.Delete(acceso.Id);
        }


        private Login Map
        {
            get
            {
                var user = new DomainModel.Login
                {
                    Id = int.Parse(txtId.Text),
                    Usuario = txtUser.Text,
                    Contrasena = txtContra.Text,
                    TipoAcceso = (int)(cbxTipoAcceso.SelectedValue),
                    Estado = (bool)cbxEstado.SelectedValue
                };
                return user;
            }
        }

        private void ReverseMap()
        {
            MessageBox.Show(acceso.ToString());
            if (acceso == null) return;
            
            txtId.Text = acceso.Id.ToString();
            txtUser.Text = acceso.Usuario;
            txtContra.Text = acceso.Contrasena;
            cbxTipoAcceso.Text = acceso.TipoAcceso.ToString();
            cbxEstado.SelectedValue = acceso.Estado;
        }

        #endregion

        #region Event

     

    
        private void AddAcceso_Load(object sender, EventArgs e)
        {
            ReverseMap();
        }


        #endregion

        private void btnEliminar_Click_1(object sender, EventArgs e)
        {
            try
            {
                Inactivar();
                MetroMessageBox.Show(this, "Los datos se eliminaron correctamente.", "Accesos", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
                Close();
            }
            catch (Exception ex)
            {
                MetroMessageBox.Show(this, ex.Message, "Accesos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
