﻿using MetroFramework.Forms;
using System;
using System.Windows.Forms;
using MetroFramework.Controls;

namespace Contabilidad_App
{
    public partial class Home : MetroForm
    {
        public string ActualUser { get; set; }
        public int NivelAcceso { get; set; }

        public Home()
        {
            InitializeComponent();

          
        }

        public void AddUserControl(TabPage tabPage, MetroUserControl userControl)
        { 
            userControl.Dock = DockStyle.Fill;
            tabPage.Controls.Add(userControl);
        }

        private void Home_Load(object sender, EventArgs e)
        {
            txtActualUser.Text = ActualUser;

            if (NivelAcceso == 2) {

                EnableTab(metroTabPage6, false);
                EnableTab(metroTabPage5, false);

            } else if (NivelAcceso==1) {

                EnableTab(metroTabPage1,false);
                EnableTab(metroTabPage6, false);
                EnableTab(metroTabPage3, false);
                EnableTab(metroTabPage7, false);



            }
        }

        public  void EnableTab(TabPage page, bool enable)
        {
            ((Control)page).Enabled = false;
        }



        private void button1_Click(object sender, EventArgs e)
        {
        
            string message = "Esta seguro que quiere salir?";
            string caption = "Cerrar Sesion";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;

            // Displays the MessageBox.

            result = MessageBox.Show(message, caption, buttons);

            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                var login = new AccesoUsuarios();
                login.Show();
                this.Hide();
                this.Close();

            }

       
        }
    }
}
