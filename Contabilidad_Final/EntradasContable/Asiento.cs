﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DomainModel;
using MetroFramework.Controls;
using Service.DetallesContables;
using Service.EntradasContables;

namespace Contabilidad_App.EntradasContable
{
    public partial class Asiento : MetroUserControl
    {
        private IEntradaContableService _asientoService;
        private IDetalleContableService _detalleAsientoService;
        public Asiento()
        {
            InitializeComponent();
        }


        private void CargarDatos(IEnumerable<EntradaContable> entradas)
        {
            dgvAsientos.Rows.Clear();
            foreach (var item in entradas)
            {
                var estado = item.Estado ? "Activo" : "Inactivo";
                dgvAsientos.Rows.Add(item.Id, item.Descripcion, item.Fecha.ToShortDateString(), item.Moneda?.Descripcion, item.Auxiliar?.Descripcion, item.Tasa, estado);
            }

        }

        private EntradaContable GetEntradaContable()
        {
            var moneda = new EntradaContable();
            var row = dgvAsientos.SelectedRows[0];
            if (row.Cells[0].Value == null)
                return moneda;

            var id = Convert.ToInt32(row.Cells[0].Value.ToString());
            moneda = _asientoService.GetOne(id);
            moneda.Detalles = _detalleAsientoService.GetAllByEntrada(id).ToList();
            return moneda;
        }

        private void Reload()
        {
            CargarDatos(_asientoService.GetAll());
        }

        private void Asiento_Load(object sender, EventArgs e)
        {
            _asientoService = new EntradaContableService();
            _detalleAsientoService = new DetalleContableService();
            var cuentas = _asientoService.GetAll();
            CargarDatos(cuentas);
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            var filtro = txtFiltro.Text;
            var datos = _asientoService.Search(filtro);
            CargarDatos(datos);

        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            var formulario = new AddAsiento { Text = "Agregar Asiento" };
            var resultado = formulario.ShowDialog();

            if (resultado == DialogResult.OK)
            {
                Reload();
                dgvAsientos.Refresh();
            }
        }

        private void dgvAsientos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var modelo = GetEntradaContable();
            var formulario = new AddAsiento { Text = "Editar Asiento", Asiento = modelo };
            var resultado = formulario.ShowDialog();

            if (resultado == DialogResult.OK)
            {
                Reload();
                dgvAsientos.Refresh();
            }

        }

        private void txtFiltro_Click(object sender, EventArgs e)
        {

        }
    }
}
