﻿namespace Contabilidad_App.CuentasContables
{
    partial class AddCuentaContable
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNoControl = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.cbxTipoCuenta = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.cbxCuentaMayor = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.cbxEstado = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.txtNoCuenta = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.txtNivel = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.txtDescripcion = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.txtId = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.btnGuardar = new MetroFramework.Controls.MetroButton();
            this.btnEliminar = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // txtNoControl
            // 
            // 
            // 
            // 
            this.txtNoControl.CustomButton.Image = null;
            this.txtNoControl.CustomButton.Location = new System.Drawing.Point(153, 1);
            this.txtNoControl.CustomButton.Name = "";
            this.txtNoControl.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtNoControl.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNoControl.CustomButton.TabIndex = 1;
            this.txtNoControl.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNoControl.CustomButton.UseSelectable = true;
            this.txtNoControl.CustomButton.Visible = false;
            this.txtNoControl.Lines = new string[] {
        "1"};
            this.txtNoControl.Location = new System.Drawing.Point(141, 189);
            this.txtNoControl.MaxLength = 18;
            this.txtNoControl.Name = "txtNoControl";
            this.txtNoControl.PasswordChar = '\0';
            this.txtNoControl.ReadOnly = true;
            this.txtNoControl.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNoControl.SelectedText = "";
            this.txtNoControl.SelectionLength = 0;
            this.txtNoControl.SelectionStart = 0;
            this.txtNoControl.ShortcutsEnabled = true;
            this.txtNoControl.Size = new System.Drawing.Size(175, 23);
            this.txtNoControl.TabIndex = 20;
            this.txtNoControl.Text = "1";
            this.txtNoControl.UseSelectable = true;
            this.txtNoControl.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNoControl.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(24, 189);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(109, 19);
            this.metroLabel8.TabIndex = 19;
            this.metroLabel8.Text = "Numero Control:";
            // 
            // cbxTipoCuenta
            // 
            this.cbxTipoCuenta.FormattingEnabled = true;
            this.cbxTipoCuenta.ItemHeight = 23;
            this.cbxTipoCuenta.Location = new System.Drawing.Point(141, 264);
            this.cbxTipoCuenta.Name = "cbxTipoCuenta";
            this.cbxTipoCuenta.Size = new System.Drawing.Size(175, 29);
            this.cbxTipoCuenta.TabIndex = 18;
            this.cbxTipoCuenta.UseSelectable = true;
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(23, 264);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(83, 19);
            this.metroLabel7.TabIndex = 17;
            this.metroLabel7.Text = "Tipo Cuenta:";
            // 
            // cbxCuentaMayor
            // 
            this.cbxCuentaMayor.FormattingEnabled = true;
            this.cbxCuentaMayor.ItemHeight = 23;
            this.cbxCuentaMayor.Location = new System.Drawing.Point(141, 223);
            this.cbxCuentaMayor.Name = "cbxCuentaMayor";
            this.cbxCuentaMayor.Size = new System.Drawing.Size(175, 29);
            this.cbxCuentaMayor.TabIndex = 16;
            this.cbxCuentaMayor.UseSelectable = true;
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(23, 223);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(96, 19);
            this.metroLabel4.TabIndex = 14;
            this.metroLabel4.Text = "Cuenta Mayor:";
            // 
            // cbxEstado
            // 
            this.cbxEstado.FormattingEnabled = true;
            this.cbxEstado.ItemHeight = 23;
            this.cbxEstado.Location = new System.Drawing.Point(479, 462);
            this.cbxEstado.Name = "cbxEstado";
            this.cbxEstado.Size = new System.Drawing.Size(175, 29);
            this.cbxEstado.TabIndex = 13;
            this.cbxEstado.UseSelectable = true;
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(361, 462);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(51, 19);
            this.metroLabel6.TabIndex = 12;
            this.metroLabel6.Text = "Estado:";
            // 
            // txtNoCuenta
            // 
            // 
            // 
            // 
            this.txtNoCuenta.CustomButton.Image = null;
            this.txtNoCuenta.CustomButton.Location = new System.Drawing.Point(153, 1);
            this.txtNoCuenta.CustomButton.Name = "";
            this.txtNoCuenta.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtNoCuenta.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNoCuenta.CustomButton.TabIndex = 1;
            this.txtNoCuenta.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNoCuenta.CustomButton.UseSelectable = true;
            this.txtNoCuenta.CustomButton.Visible = false;
            this.txtNoCuenta.Lines = new string[] {
        "0"};
            this.txtNoCuenta.Location = new System.Drawing.Point(141, 154);
            this.txtNoCuenta.MaxLength = 18;
            this.txtNoCuenta.Name = "txtNoCuenta";
            this.txtNoCuenta.PasswordChar = '\0';
            this.txtNoCuenta.ReadOnly = true;
            this.txtNoCuenta.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNoCuenta.SelectedText = "";
            this.txtNoCuenta.SelectionLength = 0;
            this.txtNoCuenta.SelectionStart = 0;
            this.txtNoCuenta.ShortcutsEnabled = true;
            this.txtNoCuenta.Size = new System.Drawing.Size(175, 23);
            this.txtNoCuenta.TabIndex = 11;
            this.txtNoCuenta.Text = "0";
            this.txtNoCuenta.UseSelectable = true;
            this.txtNoCuenta.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNoCuenta.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(23, 158);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(106, 19);
            this.metroLabel5.TabIndex = 10;
            this.metroLabel5.Text = "Numero Cuenta:";
            // 
            // txtNivel
            // 
            // 
            // 
            // 
            this.txtNivel.CustomButton.Image = null;
            this.txtNivel.CustomButton.Location = new System.Drawing.Point(153, 1);
            this.txtNivel.CustomButton.Name = "";
            this.txtNivel.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtNivel.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtNivel.CustomButton.TabIndex = 1;
            this.txtNivel.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtNivel.CustomButton.UseSelectable = true;
            this.txtNivel.CustomButton.Visible = false;
            this.txtNivel.Lines = new string[] {
        "1"};
            this.txtNivel.Location = new System.Drawing.Point(141, 122);
            this.txtNivel.MaxLength = 100;
            this.txtNivel.Name = "txtNivel";
            this.txtNivel.PasswordChar = '\0';
            this.txtNivel.ReadOnly = true;
            this.txtNivel.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNivel.SelectedText = "";
            this.txtNivel.SelectionLength = 0;
            this.txtNivel.SelectionStart = 0;
            this.txtNivel.ShortcutsEnabled = true;
            this.txtNivel.Size = new System.Drawing.Size(175, 23);
            this.txtNivel.TabIndex = 7;
            this.txtNivel.Text = "1";
            this.txtNivel.UseSelectable = true;
            this.txtNivel.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtNivel.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(23, 126);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(41, 19);
            this.metroLabel3.TabIndex = 6;
            this.metroLabel3.Text = "Nivel:";
            // 
            // txtDescripcion
            // 
            // 
            // 
            // 
            this.txtDescripcion.CustomButton.Image = null;
            this.txtDescripcion.CustomButton.Location = new System.Drawing.Point(153, 1);
            this.txtDescripcion.CustomButton.Name = "";
            this.txtDescripcion.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtDescripcion.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtDescripcion.CustomButton.TabIndex = 1;
            this.txtDescripcion.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtDescripcion.CustomButton.UseSelectable = true;
            this.txtDescripcion.CustomButton.Visible = false;
            this.txtDescripcion.Lines = new string[0];
            this.txtDescripcion.Location = new System.Drawing.Point(141, 89);
            this.txtDescripcion.MaxLength = 100;
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.PasswordChar = '\0';
            this.txtDescripcion.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtDescripcion.SelectedText = "";
            this.txtDescripcion.SelectionLength = 0;
            this.txtDescripcion.SelectionStart = 0;
            this.txtDescripcion.ShortcutsEnabled = true;
            this.txtDescripcion.Size = new System.Drawing.Size(175, 23);
            this.txtDescripcion.TabIndex = 5;
            this.txtDescripcion.UseSelectable = true;
            this.txtDescripcion.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtDescripcion.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(23, 93);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(79, 19);
            this.metroLabel2.TabIndex = 4;
            this.metroLabel2.Text = "Descripcion:";
            // 
            // txtId
            // 
            // 
            // 
            // 
            this.txtId.CustomButton.Image = null;
            this.txtId.CustomButton.Location = new System.Drawing.Point(153, 1);
            this.txtId.CustomButton.Name = "";
            this.txtId.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtId.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtId.CustomButton.TabIndex = 1;
            this.txtId.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtId.CustomButton.UseSelectable = true;
            this.txtId.CustomButton.Visible = false;
            this.txtId.Lines = new string[] {
        "0"};
            this.txtId.Location = new System.Drawing.Point(141, 56);
            this.txtId.MaxLength = 32767;
            this.txtId.Name = "txtId";
            this.txtId.PasswordChar = '\0';
            this.txtId.ReadOnly = true;
            this.txtId.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtId.SelectedText = "";
            this.txtId.SelectionLength = 0;
            this.txtId.SelectionStart = 0;
            this.txtId.ShortcutsEnabled = true;
            this.txtId.Size = new System.Drawing.Size(175, 23);
            this.txtId.TabIndex = 3;
            this.txtId.Text = "0";
            this.txtId.UseSelectable = true;
            this.txtId.Visible = false;
            this.txtId.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtId.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(23, 60);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(23, 19);
            this.metroLabel1.TabIndex = 2;
            this.metroLabel1.Text = "Id:";
            this.metroLabel1.Visible = false;
            // 
            // btnGuardar
            // 
            this.btnGuardar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnGuardar.Location = new System.Drawing.Point(356, 93);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(100, 49);
            this.btnGuardar.TabIndex = 1;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseSelectable = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnEliminar.Location = new System.Drawing.Point(356, 148);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(100, 51);
            this.btnEliminar.TabIndex = 2;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseSelectable = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // AddCuentaContable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(479, 384);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.txtNoControl);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.metroLabel8);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.cbxTipoCuenta);
            this.Controls.Add(this.txtId);
            this.Controls.Add(this.metroLabel7);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.cbxCuentaMayor);
            this.Controls.Add(this.txtDescripcion);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.cbxEstado);
            this.Controls.Add(this.txtNivel);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.txtNoCuenta);
            this.Name = "AddCuentaContable";
            this.Style = MetroFramework.MetroColorStyle.Green;
            this.Text = "AddCuentaContable";
            this.Theme = MetroFramework.MetroThemeStyle.Default;
            this.Load += new System.EventHandler(this.AddCuentaContable_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MetroFramework.Controls.MetroComboBox cbxEstado;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroTextBox txtNoCuenta;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroTextBox txtNivel;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroTextBox txtDescripcion;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox txtId;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroButton btnGuardar;
        private MetroFramework.Controls.MetroButton btnEliminar;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroTextBox txtNoControl;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroComboBox cbxTipoCuenta;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroComboBox cbxCuentaMayor;
    }
}