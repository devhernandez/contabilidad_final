﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using MetroFramework.Controls;
using Service.CuentasContables;

namespace Contabilidad_App.CuentasContables
{
    public partial class CuentaContable : MetroUserControl
    {
        private ICuentaContableService _cuentaContableService;
        public CuentaContable()
        {
            InitializeComponent();
            Reload();
        }

        private void CargarDatos(IEnumerable<DomainModel.CuentaContable> cuentas)
        {
            dgvCuentas.Rows.Clear();
            foreach (var item in cuentas)
            {
                var estado = item.Estado ? "Activo" : "Inactivo";
                dgvCuentas.Rows.Add(item.Id, item.Descripcion, item.Nivel, item.NoCuenta, item.CuentaMayor?.Descripcion, item.TipoCuenta?.Descripcion ,item.Balance ,estado);
            }

        }

        private DomainModel.CuentaContable GetCuenta()
        {
            var moneda = new DomainModel.CuentaContable();
            var row = dgvCuentas.SelectedRows[0];
            if (row.Cells[0].Value == null)
                return moneda;

            moneda = _cuentaContableService.GetOne(Convert.ToInt32(row.Cells[0].Value.ToString()));
            return moneda;
        }

        private void Reload()
        { _cuentaContableService = new CuentaContableService();
            CargarDatos(_cuentaContableService.GetAll());
        }

        private void CuentaContable_Load(object sender, EventArgs e)
        {
            _cuentaContableService = new CuentaContableService();
            var cuentas = _cuentaContableService.GetAll();
            CargarDatos(cuentas);
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            var filtro = txtFiltro.Text;
            var datos = _cuentaContableService.Search(filtro);
            CargarDatos(datos);

        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            var formulario = new AddCuentaContable { Text = "Agregar Cuenta Contable"};
            var resultado = formulario.ShowDialog();

            if (resultado == DialogResult.OK)
            {
                Reload();
                dgvCuentas.Refresh();
            }
        }

        private void dgvCuentas_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var modelo = GetCuenta();
            var formulario = new AddCuentaContable { Text = "Editar Cuenta Contable", CuentaContable = modelo };
            var resultado = formulario.ShowDialog();

            if (resultado == DialogResult.OK)
            {
                Reload();
                dgvCuentas.Refresh();
            }
        }
    }
}
