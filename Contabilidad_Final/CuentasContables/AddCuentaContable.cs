﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using MetroFramework;
using MetroFramework.Forms;
using Service.CuentasContables;
using Service.TiposCuenta;

namespace Contabilidad_App.CuentasContables
{
    public partial class AddCuentaContable : MetroForm
    {
        public DomainModel.CuentaContable CuentaContable { get; set; }
        private readonly ICuentaContableService _cuentaContableService;
        private readonly ITipoCuentaService _tipoCuentaService;

        public AddCuentaContable()
        {
            InitializeComponent();
            CargarEstado();
            _cuentaContableService = new CuentaContableService();
            _tipoCuentaService = new TipoCuentaService();
            CargarTipoCuenta();
            CargarCuentas();


        }

        private void CargarEstado()
        {
            var estados = new Dictionary<bool, string> { { true, "Activo" }, { false, "Inactivo" } };
            cbxEstado.DataSource = new BindingSource(estados, null);
            cbxEstado.DisplayMember = "Value";
            cbxEstado.ValueMember = "Key";
        }

        private void CargarTipoCuenta()
        {
            var tiposCuenta = _tipoCuentaService.GetAll()
                              .Select(x => new {Id = x.Id, Nombre = x.Nombre});
            cbxTipoCuenta.DataSource = new BindingSource(tiposCuenta, null);
            cbxTipoCuenta.DisplayMember = "Nombre";
            cbxTipoCuenta.ValueMember = "Id";
        }

        private void CargarCuentas()
        {
            var cuentas = _cuentaContableService.GetAll()
                              .Select(x => new { Id = x.Id, Nombre = x.Descripcion }).ToList();

            cuentas.Insert(0,  new { Id = 0, Nombre = "N/A"});
            cbxCuentaMayor.DataSource = new BindingSource(cuentas, null);
            cbxCuentaMayor.DisplayMember = "Nombre";
            cbxCuentaMayor.ValueMember = "Id";
        }

        #region CRUD

        private void Add(DomainModel.CuentaContable cuenta)
        {
            if (cuenta.CuentaMayorId == 0)
                cuenta.CuentaMayorId = null;
            cuenta.Balance = CuentaContable.Balance;
            _cuentaContableService.Save(cuenta);
        }

        #endregion

        #region Action

        private void Agregar()
        {
            var tipoCuenta = Map;
            Add(tipoCuenta);

        }

        private void Inactivar()
        {
            var tipoCuenta = Map;
            _cuentaContableService.Delete(tipoCuenta.Id);
        }

        private DomainModel.CuentaContable Map
        {
            get
            {
                var model = new DomainModel.CuentaContable
                {
                    Id = int.Parse(txtId.Text),
                    Descripcion = txtDescripcion.Text,
                    Nivel = Convert.ToInt32(txtNivel.Text),
                    NoCuenta = txtNoCuenta.Text,
                    NoControl = Convert.ToInt32(txtNoControl.Text),
                    TipoCuentaId = Convert.ToInt32(cbxTipoCuenta.SelectedValue),
                    Estado = (bool)cbxEstado.SelectedValue
                };
                model.CuentaMayorId = cbxCuentaMayor.SelectedValue != null
                    ? Convert.ToInt32(cbxCuentaMayor.SelectedValue)
                    : 0;
                return model;
            }
        }

        private void ReverseMap()
        {
            if (CuentaContable == null) return;

            txtId.Text = CuentaContable.Id.ToString();
            txtDescripcion.Text = CuentaContable.Descripcion;
            txtNivel.Text = CuentaContable.Nivel.ToString();
            txtNoCuenta.Text = CuentaContable.NoCuenta;
            txtNoControl.Text = CuentaContable.NoControl.ToString();
            cbxCuentaMayor.SelectedValue = CuentaContable.CuentaMayorId ?? 0;
            cbxTipoCuenta.SelectedValue = CuentaContable.TipoCuentaId;
            cbxEstado.SelectedValue = CuentaContable.Estado;
        }

        #endregion

        private void AddCuentaContable_Load(object sender, EventArgs e)
        {
            ReverseMap();
            CuentaContable = new DomainModel.CuentaContable();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Agregar();
                MetroMessageBox.Show(this, "Listo!", "Cuenta Contable", MessageBoxButtons.OK, MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
                Close();
            }
            catch (Exception ex)
            {
                MetroMessageBox.Show(this, ex.Message, "Cuenta Contable", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                Inactivar();
                MetroMessageBox.Show(this, "Los datos se eliminaron correctamente.", "Cuenta Contable", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
                Close();
            }
            catch (Exception ex)
            {
                MetroMessageBox.Show(this, ex.Message, "Cuenta Contable", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
   
    }
}
