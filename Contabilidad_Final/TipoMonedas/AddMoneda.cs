﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;
using Contabilidad_App.Utils;
using DomainModel;
using MetroFramework;
using MetroFramework.Forms;
using Service.Monedas;

namespace Contabilidad_App.TipoMonedas
{
    public partial class AddMoneda : MetroForm
    {
        public TipoMoneda TipoMoneda { get; set; }
        private readonly ITipoMonedaService _tipoMonedaService;

        public AddMoneda()
        {
            InitializeComponent();
            CargarEstado();
            _tipoMonedaService = new TipoMonedaService();
        }

        private void CargarEstado()
        {
            var estados = new Dictionary<bool, string> { { true, "Activo" }, { false, "Inactivo" } };
            cbxEstado.DataSource = new BindingSource(estados, null);
            cbxEstado.DisplayMember = "Value";
            cbxEstado.ValueMember = "Key";
        }


        #region CRUD

        private void Add(DomainModel.TipoMoneda moneda)
        {
            _tipoMonedaService.Save(moneda);
        }

        #endregion

        #region Action

        private void Agregar()
        {
            var tipoCuenta = Map;
            Add(tipoCuenta);

        }

        private void Inactivar()
        {
            var tipoCuenta = Map;
            _tipoMonedaService.Delete(tipoCuenta.Id);
        }

        private TipoMoneda Map
        {
            get
            {
                var moneda = new DomainModel.TipoMoneda
                {
                    Id = int.Parse(txtId.Text),
                    Formato = txtFormato.Text,
                    Descripcion = txtDescripcion.Text,
                    TasaActual = decimal.Parse(txtTasaActual.Text),
                    Estado = (bool)cbxEstado.SelectedValue
                };
                return moneda;
            }
        }

        private void ReverseMap()
        {
            if (TipoMoneda == null) return;
            
            txtId.Text = TipoMoneda.Id.ToString();
            txtDescripcion.Text = TipoMoneda.Descripcion;
            txtFormato.Text = TipoMoneda.Formato;
            txtTasaActual.Text = TipoMoneda.TasaActual.ToString(CultureInfo.InvariantCulture);
            cbxEstado.SelectedValue = TipoMoneda.Estado;
        }

        #endregion

        #region Event

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Agregar();
                MetroMessageBox.Show(this, "Los datos se guardaron correctamente.", "Tipo Moneda", MessageBoxButtons.OK, MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
                Close();
            }
            catch (Exception ex)
            {
                MetroMessageBox.Show(this, ex.Message, "Tipo Moneda", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                Inactivar();
                MetroMessageBox.Show(this, "Los datos se eliminaron correctamente.", "Tipo Moneda", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
                Close();
            }
            catch (Exception ex)
            {
                MetroMessageBox.Show(this, ex.Message, "Tipo Moneda", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void AddMoneda_Load(object sender, EventArgs e)
        {
            ReverseMap();
        }


        #endregion

        private void txtTasaActual_KeyPress(object sender, KeyPressEventArgs e)
        {
            var validator = new TextBoxValidator();
            validator.IsDecimal(sender, e);
        }
    }
}
