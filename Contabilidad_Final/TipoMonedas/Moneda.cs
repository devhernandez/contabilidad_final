﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DomainModel;
using MetroFramework.Controls;
using Service.Monedas;

namespace Contabilidad_App.TipoMonedas
{
    public partial class Moneda : MetroUserControl
    {
        private ITipoMonedaService _tipoMonedaService;
        public Moneda()
        {
            InitializeComponent();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            var filtro = txtFiltro.Text;
            var monedas = _tipoMonedaService.Search(filtro);
            CargarDatos(monedas);

        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            var formulario = new AddMoneda();
            formulario.ShowDialog();
        }

        private void CargarDatos(IEnumerable<TipoMoneda> monedas)
        {
            dgvMonedas.Rows.Clear();
            foreach (var moneda in monedas)
            {
                var estado = moneda.Estado ? "Activo" : "Inactivo";
                dgvMonedas.Rows.Add(moneda.Id, moneda.Descripcion, moneda.Formato, moneda.TasaActual, estado);
            }
        }

        private TipoMoneda GetMoneda()
        {
            var moneda = new TipoMoneda();
            var row = dgvMonedas.SelectedRows[0];
            if (row.Cells[0].Value == null)
                return moneda;

            moneda = _tipoMonedaService.GetOne(Convert.ToInt32(row.Cells[0].Value.ToString()));
            return moneda;
        }

        private void Reload()
        {
            CargarDatos(_tipoMonedaService.GetAll());
        }

        private void dgvTiposCuentas_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var moneda = GetMoneda();
            var formulario = new AddMoneda { Text = "Editar Moneda", TipoMoneda = moneda };
            var resultado = formulario.ShowDialog();

            if (resultado == DialogResult.OK)
            {
                Reload();
                dgvMonedas.Refresh();
            }
        }

        private void Moneda_Load(object sender, EventArgs e)
        {
            _tipoMonedaService = new TipoMonedaService();
            var monedas = _tipoMonedaService.GetAll();
            CargarDatos(monedas);

        }

        private void metroPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void metroPanel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dgvMonedas_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void metroPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void metroPanel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void metroLabel1_Click(object sender, EventArgs e)
        {

        }

        private void txtFiltro_Click(object sender, EventArgs e)
        {

        }

        private void metroPanel5_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
