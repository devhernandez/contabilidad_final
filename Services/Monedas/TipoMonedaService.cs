﻿using System.Collections.Generic;
using DomainModel;
using FluentValidation;
using Service.Common;
using Service.Validations;

namespace Service.Monedas
{
    public class TipoMonedaService : ServiceBase<TipoMoneda>, ITipoMonedaService
    {
        public IEnumerable<TipoMoneda> Search(string filtro)
        {
            return GetAll(x => x.Descripcion.StartsWith(filtro) || x.Formato.StartsWith(filtro) ||
                               x.Id.ToString().StartsWith(filtro));
        }

        public override void Save(TipoMoneda entity)
        {
            var validator = new MonedaValidator();
            validator.ValidateAndThrow(entity);
            base.Save(entity);
        }
    }
}
