﻿using System.Collections.Generic;
using DomainModel;
using Service.Common;

namespace Service.Monedas
{
    public interface ITipoMonedaService : IService<TipoMoneda>
    {
        IEnumerable<TipoMoneda> Search(string filtro);
    }
}
