﻿using System;
using System.Collections.Generic;
using DomainModel;
using FluentValidation;
using Service.Common;
using Service.DetallesContables;
using Service.Mayorizacion;
using Service.Validations;

namespace Service.EntradasContables
{
    public class EntradaContableService : ServiceBase<EntradaContable>, IEntradaContableService
    {
        
        private IDetalleContableService _detalleContableService;
        private IMayorizacionService _mayorizacionService;

        public IEnumerable<EntradaContable> Search(string filtro)
        {
            return GetAll(x => x.Descripcion.StartsWith(filtro) || x.Auxiliar.Descripcion.StartsWith(filtro) ||
                               x.Moneda.Descripcion.StartsWith(filtro));
        }

        public override void Add(EntradaContable entity)
        {
            _mayorizacionService = new MayorizacionService();
            base.Add(entity);

            foreach (var item in entity.Detalles)
            {
                _mayorizacionService.Mayorizar(item);
            }
        }

        public override void Update(EntradaContable entity)
        {
            _mayorizacionService = new MayorizacionService();
            _detalleContableService = new DetalleContableService();

            base.Update(entity);

            foreach (var item in entity.Detalles)
            {
                var detalle = _detalleContableService.GetOne(item.Id);
                var montoActual = item.Debito - item.Credito;
                var montoAnterior = detalle.Debito - detalle.Credito;
                _detalleContableService.Update(item);
                _mayorizacionService.ActualizarMayor(item.CuentaId, montoAnterior, montoActual);
            }
        }

        public override void Delete(int id)
        {
            _mayorizacionService = new MayorizacionService();
            _detalleContableService = new DetalleContableService();

          //  var entity = GetOne(id);
            foreach (var item in _detalleContableService.GetAllByEntrada(id))
            {
                var detalle = _detalleContableService.GetOne(item.Id);
                var montoActual = 0;
                var montoAnterior = detalle.Debito - detalle.Credito;
                _detalleContableService.Update(item);
                _mayorizacionService.ActualizarMayor(item.CuentaId, montoAnterior, montoActual);
            }

            base.Delete(id);
        }

        public override void Save(EntradaContable entity)
        {
            var validator = new EntradaContableValidator();
            validator.ValidateAndThrow(entity);
            var action = Exists(entity.Id) ? new Action<EntradaContable>(Update) : Add;
            action(entity);
        }
    }
}
