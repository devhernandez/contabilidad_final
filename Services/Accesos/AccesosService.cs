﻿using System.Collections.Generic;
using DomainModel;
using FluentValidation;
using Service.Common;
using Service.Validations;

namespace Service.Accesos
{
    public class AccesosService: ServiceBase<Login>, IAccesosService
    {
        public IEnumerable<Login> Search(string filtro)
        {
            return GetAll(x => x.Id.ToString().StartsWith(filtro) || x.Usuario.Contains(filtro));
        }

        public override void Save(Login entity)
        {
            var loginValidator = new AccesosValidator();
            loginValidator.ValidateAndThrow(entity);
            base.Save(entity);
        }

    }
}
