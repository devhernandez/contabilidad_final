﻿using System.Collections.Generic;
using DomainModel;
using Service.Common;

namespace Service.Accesos
{
   public interface IAccesosService: IService<Login>
    {
        IEnumerable<Login> Search(string filtro);
    }
}
