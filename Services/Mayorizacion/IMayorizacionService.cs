﻿using System.Collections.Generic;
using DomainModel;
using Service.Common;

namespace Service.Mayorizacion
{
    public interface IMayorizacionService : IService<DomainModel.Mayorizacion>
    {
        IEnumerable<DomainModel.Mayorizacion> Search(int anio, int mes);
        void Mayorizar(int anio, int mes);
        void Mayorizar(DetalleEntrada detalleEntrada);
        void ActualizarMayor(int cuentaId, decimal montoAnterior, decimal montoActual);

    }
}
