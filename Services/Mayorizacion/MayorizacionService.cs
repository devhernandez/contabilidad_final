﻿using System.Collections.Generic;
using DomainModel;
using Service.Common;
using Service.CuentasContables;
using Service.EntradasContables;

namespace Service.Mayorizacion
{
    public class MayorizacionService : ServiceBase<DomainModel.Mayorizacion>, IMayorizacionService
    {
        private ICuentaContableService _cuentaService;
        private IEntradaContableService _asientoService;

        public MayorizacionService()
        {
            
        }

        public IEnumerable<DomainModel.Mayorizacion> Search(int anio, int mes)
        {
            return GetAll(x => x.Fecha.Year == anio && x.Fecha.Month == mes);
        }

        public void Mayorizar(int anio, int mes)
        {
            _asientoService = new EntradaContableService();
            var diarioGeneral = _asientoService.GetAll(x => x.Fecha.Year == anio && x.Fecha.Month == mes);


        }

        public void Mayorizar(DetalleEntrada detalleEntrada)
        {
            var monto = detalleEntrada.Debito - detalleEntrada.Credito;
            Mayorizar(detalleEntrada.CuentaId, monto);
        }

        private void Mayorizar(int cuentaId, decimal monto)
        {
            _cuentaService = new CuentaContableService();
            var cuenta = _cuentaService.GetOne(cuentaId);
            cuenta.Balance += monto;
            _cuentaService.Update(cuenta);
            if (cuenta.CuentaMayorId != null)
            {
                Mayorizar(cuenta.CuentaMayorId.Value, monto);
            }
        }

        public void ActualizarMayor(int cuentaId, decimal montoAnterior, decimal montoActual)
        {
            _cuentaService = new CuentaContableService();
            var cuenta = _cuentaService.GetOne(cuentaId);
            cuenta.Balance = cuenta.Balance - montoAnterior + montoActual;
            _cuentaService.Update(cuenta);
            if (cuenta.CuentaMayorId != null)
            {
                ActualizarMayor(cuenta.CuentaMayorId.Value, montoAnterior, montoActual);
            }
        }
    }
}
