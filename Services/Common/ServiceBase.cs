﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using DomainModel.Base;
using Repository;

namespace Service.Common
{
    /*T: Domain Model
     M: DTO Model,
     E: Repository*/
    public abstract class ServiceBase<T> : IService<T> 
        where T : BaseDomainModel
    {
        private readonly IGenericRepository<T> _repository;

        protected ServiceBase()
        {
            //AutoMapper.Mapper.CreateMap<T, M>();
            //AutoMapper.Mapper.CreateMap<M, T>();
            Mapper.Initialize(config =>
            {
                config.CreateMissingTypeMaps = true;
            });
            _repository = new GenericRepository<T>();
        }

        public T GetSingle(Expression<Func<T, bool>> predicate)
        {
            var model = _repository.GetSingle(predicate);
            return model;

        }

        public virtual void Add(T entity)
        {
            _repository.Add(entity);
        }

        public void Delete(T entity)
        {
            ValidarExistencia(entity.Id);
            _repository.Delete(entity);
        }

        public virtual void Update(T entity)
        {
            ValidarExistencia(entity.Id);
            _repository.Update(entity);

        }

        public List<T> GetAll(Expression<Func<T, bool>> predicate)
        {
            return _repository.GetAll(predicate).ToList();
        }

        public List<T> GetAll()
        {
            return _repository.GetAll(x => x.Estado).ToList();
        }

        public IQueryable<T> Query(Expression<Func<T, bool>> whereCondition)
        {
            return _repository.Query(whereCondition).AsQueryable();
        }

        public long Count(Expression<Func<T, bool>> whereCondition)
        {
            return _repository.Count(whereCondition);
        }

        public long Count()
        {
            return _repository.Count();
        }

        public bool Exists(int id)
        {
            return _repository.Exists(id);
        }

        public bool Exists(Expression<Func<T, bool>> whereCondition)
        {
            return _repository.Exists(whereCondition);
        }

        public virtual void Save(T entity)
        {
            var action = Exists(entity.Id) ? new Action<T>(Update) : Add;
            action(entity);
        }

        public T GetOne(int id)
        {
            ValidarExistencia(id);

            return _repository.GetOne(id);
        }

        public virtual void Delete(int id)
        {
            ValidarExistencia(id);

            var model = GetOne(id);
            model.Estado = false;
            Update(model);
        }

        private void ValidarExistencia(int id)
        {
            if (!Exists(id)) throw new Exception("No existe este registro.");
        }
    }
}
