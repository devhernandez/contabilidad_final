﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Service.Common
{
    public interface IService< T>
    {
        T GetSingle(Expression<Func<T, bool>> whereCondition);

        void Add(T entity);

        void Delete(T entity);

        void Update(T entity);

        void Save(T entity);

        List<T> GetAll(Expression<Func<T, bool>> whereCondition);

        List<T> GetAll();

        IQueryable<T> Query(Expression<Func<T, bool>> whereCondition);

        long Count(Expression<Func<T, bool>> whereCondition);

        long Count();

        bool Exists(int id);

        bool Exists(Expression<Func<T, bool>> whereCondition);

        T GetOne(int id);

        void Delete(int id);

    }
}
