﻿using DomainModel;
using Service.Common;

namespace Service.TiposMovimientos
{
    public interface ITipoMovimientoService : IService<TipoMovimiento>
    {
    }
}
