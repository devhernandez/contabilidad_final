﻿namespace Service.DTO
{
    public class AuxiliarDto
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public bool Estado { get; set; }
    }
}
