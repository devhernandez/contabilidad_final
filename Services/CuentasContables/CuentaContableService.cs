﻿using System;
using System.Collections.Generic;
using DomainModel;
using FluentValidation;
using Service.Common;
using Service.TiposCuenta;
using Service.Validations;
using System.Linq;

namespace Service.CuentasContables
{
    public class CuentaContableService : ServiceBase<CuentaContable> , ICuentaContableService
    {
        public IEnumerable<CuentaContable> Search(string filtro)
        {
            return GetAll(x => x.Descripcion.StartsWith(filtro) || x.CuentaMayor.Descripcion.StartsWith(filtro) ||
                               x.TipoCuenta.Nombre.StartsWith(filtro) || x.TipoCuenta.Descripcion.StartsWith(filtro));
        }

        public override void Add(CuentaContable entity)
        {
            var nivel = 1;
            string noCuenta;

            
            if (entity.CuentaMayorId != 0 && entity.CuentaMayorId != null)
            {
                var cuentaMayor = GetOne((int) entity.CuentaMayorId);
                nivel = cuentaMayor.Nivel + 1;
                 noCuenta = GenerarNoControl(cuentaMayor.Id);
                entity.PermiteTransacciones = true;
            }
            else
            {
                noCuenta = GenerarNoCuentaByTipoCuenta(entity.TipoCuentaId);
            }

            entity.Nivel = nivel;
            entity.NoCuenta = noCuenta;
            base.Add(entity);
        }

        private string GenerarNoControl(int id)
        {
            var noCuenta = "";
            if (!Exists(id)) return noCuenta;

            var cuentaMayor = GetOne(id);
            noCuenta = cuentaMayor.NoCuenta + cuentaMayor.NoControl;
            ActualizarCuentaMayor(id);

            return noCuenta;
        }

        private void ActualizarCuentaMayor(int id)
        {
            var cuentaMayor = GetOne(id);
            cuentaMayor.PermiteTransacciones = false;
            cuentaMayor.NoControl += 1;
            Update(cuentaMayor);
        }

        private static string GenerarNoCuentaByTipoCuenta(int id)
        {
            var noCuenta = "0";
            var tipoCuentaService = new TipoCuentaService();
            if (!tipoCuentaService.Exists(id)) return noCuenta;

            var tipoCuenta = tipoCuentaService.GetOne(id);
            noCuenta = tipoCuenta.NoControl.ToString();
            return noCuenta;
        }

        public override void Save(CuentaContable entity)
        {
            var validator = new CuentaContableValidator();
            validator.ValidateAndThrow(entity);

            var action = Exists(entity.Id) ? new Action<CuentaContable>(Update) : Add;
            action(entity);
        }

      
    }
}
