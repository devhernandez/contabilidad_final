﻿using System;
using System.Linq;
using DomainModel;
using FluentValidation;

namespace Service.Validations
{
    public class EntradaContableValidator : AbstractValidator<EntradaContable>
    {
        public EntradaContableValidator()
        {
            RuleFor(item => item.Descripcion).NotEmpty().WithMessage("La descripcion es obligatoria.")
                .MaximumLength(100).WithMessage("La descripcion no puede tener mas de 100 caracteres.");

            RuleFor(item => item.Detalles).Must(list => list.Count >= 1).WithMessage("El asiento debe afectar por lo menos 2 cuentas.")
                 .SetCollectionValidator(new DetalleContableValidator());

            RuleFor(item => item.Fecha).Must(IsValidDate).WithMessage("La fecha es obligatoria.");

            //RuleFor(item => item.Tasa).NotNull().WithMessage("La tasa es obligatoria.")
            //    .LessThan(1).WithMessage("La tasa debe ser mayor a cero");

            RuleFor(item => item).Must(IsValidAsiento)
                .WithMessage(
                    "El debito es diferente del credito.");

        }

        private bool IsValidDate(DateTime date)
        {
            return !date.Equals(default(DateTime));
        }

        private bool IsValidAsiento(EntradaContable entrada)
        {
            return entrada.Detalles.Sum(x => x.Credito) == entrada.Detalles.Sum(y => y.Debito);
        }
    }
}
