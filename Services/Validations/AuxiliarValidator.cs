﻿using DomainModel;
using FluentValidation;

namespace Service.Validations
{
    public class AuxiliarValidator : AbstractValidator<Auxiliar>
    {
        public AuxiliarValidator()
        {
            RuleFor(item => item.Descripcion).NotEmpty().WithMessage("La descripcion es obligatoria.")
                .MaximumLength(100).WithMessage("La descripcion no puede tener mas de 100 caracteres.");
        }
    }
}
