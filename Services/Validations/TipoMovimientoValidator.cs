﻿using DomainModel;
using FluentValidation;

namespace Service.Validations
{
    public class TipoMovimientoValidator : AbstractValidator<TipoMovimiento>
    {
        public TipoMovimientoValidator()
        {
            RuleFor(item => item.Nombre).NotEmpty().WithMessage("El nombre es obligatorio.")
                .MaximumLength(100).WithMessage("La descripcion no puede tener mas de 100 caracteres.");
        }
    }
}
