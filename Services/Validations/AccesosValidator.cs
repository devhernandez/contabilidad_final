﻿using DomainModel;
using FluentValidation;

namespace Service.Validations
{
    public class AccesosValidator: AbstractValidator<Login>
    {
        public AccesosValidator()
        {
            RuleFor(item => item.Usuario).NotEmpty().WithMessage("El usuario es obligatoria.")
               .MaximumLength(100).WithMessage("El usuario no puede tener mas de 100 caracteres.");


            RuleFor(item => item.Contrasena).NotEmpty().WithMessage("La contraseña es obligatoria.")
             .MaximumLength(100).WithMessage("La contraseña no puede tener mas de 100 caracteres.");

       
        }

    }
}
