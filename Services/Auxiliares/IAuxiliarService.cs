﻿using System.Collections.Generic;
using DomainModel;
using Service.Common;

namespace Service.Auxiliares
{
    public interface IAuxiliarService : IService<Auxiliar>
    {
        IEnumerable<Auxiliar> Search(string filtro);
    }
}
