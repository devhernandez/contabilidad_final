﻿using System.Collections.Generic;
using DomainModel;
using Service.Common;

namespace Service.DetallesContables
{
    public class DetalleContableService : ServiceBase<DetalleEntrada>, IDetalleContableService
    {
        public IEnumerable<DetalleEntrada> GetAllByEntrada(int entradaId)
        {
            return GetAll(x => x.EntradaContableId == entradaId);
        }
    }
}
