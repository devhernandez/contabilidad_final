using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DomainModel.Base;

namespace DomainModel
{
    [Table("TipoMovimiento")]
    public  class TipoMovimiento : BaseDomainModel
    {
        [Required]
        [StringLength(50)]
        public string Nombre { get; set; }

        public ICollection<TipoCuenta> TipoCuentas { get; set; }

    }
}
