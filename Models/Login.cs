﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DomainModel.Base;
namespace DomainModel
{
    [Table("Login")]
    public  class Login: BaseDomainModel
    {

        [Required]
        [StringLength(100)]
        public string Usuario { get; set; }

        [Required]
        [StringLength(100)]
        public string Contrasena { get; set; }

        [Required]
        public int TipoAcceso { get; set; }
    }
}
