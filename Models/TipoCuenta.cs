using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DomainModel.Base;

namespace DomainModel
{
    [Table("TipoCuenta")]
    public  class TipoCuenta : BaseDomainModel
    {

        [Required]
        [StringLength(100)]
        public string Nombre { get; set; }

        [Required]
        [StringLength(100)]
        public string Descripcion { get; set; }

        [Required]
        public int NoControl { get; set; } // = 1;

        [Required]
        public int OrigenId { get; set; }


        public virtual TipoMovimiento Origen { get; set; }

        public ICollection<CuentaContable> CuentasContables { get; set; }
    }
}
