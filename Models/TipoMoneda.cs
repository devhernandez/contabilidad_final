using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DomainModel.Base;

namespace DomainModel
{
    [Table("TipoMoneda")]
    public class TipoMoneda : BaseDomainModel
    {
        [Required]
        [StringLength(100)]
        public string Formato { get; set; }

        [Required]
        [StringLength(100)]
        public string Descripcion { get; set; }

        [Required]
        public decimal TasaActual { get; set; } = 1;

        public ICollection<EntradaContable> EntradasContables { get; set; }
    }
}
